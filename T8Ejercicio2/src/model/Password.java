package model;

import java.util.Random;

public class Password {

	// Atributos
	private int longitud;
	
	private String contraseña;

	// Constructores
	public Password() {
		this.longitud = 8;
		this.contraseña = generarContraseña(this.longitud);
	}

	public Password(int longitud) {
		this.longitud = longitud;
		this.contraseña = generarContraseña(this.longitud);
		
	}
	
	// Creo el método para que genere la contraseña aleatoria
	public static String generarContraseña (int longitud) {
		
		String contraseña = "";
		
		Random rng = new Random();
		
		for (int i = 0; i < longitud; i++) {
			contraseña += (char) (rng.nextInt(91) + 65);
		}
		
		return contraseña;
	}

	// toString
	@Override
	public String toString() {
		return "password [longitud=" + longitud + ", contraseña=" + contraseña + "]";
	}

	// Getters y Setters
	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
	
	
	
}
