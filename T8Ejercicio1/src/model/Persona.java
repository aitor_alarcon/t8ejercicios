package model;

public class Persona {

	// Constante sexo
	final char hombre = 'H';
	
	// Atributos
	private String nombre;
	
	private int edad;
	
	private String DNI;
	
	private char sexo;
	
	private double peso;
	
	private double altura;
	
	// Constructores
	public Persona() {
		this.nombre = "";
		this.edad = 0;
		this.DNI = "39493841N";
		this.sexo = hombre;
		this.peso = 0;
		this.altura = 0;
	}

	
	public Persona(String nombre, int edad, char sexo) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
		this.DNI = "39493841N";
		this.peso = 0;
		this.altura = 0;
	}


	public Persona(String nombre, int edad, String dNI, char sexo, double peso, double altura) {
		this.nombre = nombre;
		this.edad = edad;
		DNI = dNI;
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}


	// toString
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", DNI=" + DNI + ", sexo=" + sexo
				+ ", peso=" + peso + ", altura=" + altura + "]";
	}


	
	// Getters y Setters
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public String getDNI() {
		return DNI;
	}


	public void setDNI(String dNI) {
		DNI = dNI;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	public double getPeso() {
		return peso;
	}


	public void setPeso(double peso) {
		this.peso = peso;
	}


	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}


	public char getHombre() {
		return hombre;
	}
	
	
	
	
}
