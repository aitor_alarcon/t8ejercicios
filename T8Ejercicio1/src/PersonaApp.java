import model.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Persona persona1 = new Persona();
		Persona persona2 = new Persona("Vicky", 20, 'M');
		Persona persona3 = new Persona("Aitor", 20, "39871443M", 'H', 50.1, 1.66);
		
		System.out.println(persona1);
		System.out.println(persona2);
		System.out.println(persona3);
	}

}
