package model;

public class Electrodomestico {

	// Constantes
	final String colorDefecto = "Blanco";
	
	final char consumoDefecto = 'F';
	
	final double precioBaseDefecto = 100;
	
	final double pesoDefecto = 5;
	
	// Atributos
	private double precioBase;
	
	private String color;
	
	private char consumoEnergetico;
	
	private double peso;

	// Constructores
	public Electrodomestico() {
		this.precioBase = precioBaseDefecto;
		this.color = colorDefecto;
		this.consumoEnergetico = consumoDefecto;
		this.peso = pesoDefecto;
	}

	public Electrodomestico(double precioBase, double peso) {
		this.precioBase = precioBase;
		this.peso = peso;
		this.color = colorDefecto;
		this.consumoEnergetico = consumoDefecto;
	}

	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase = precioBase;
		this.color = color;
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso;
	}

	// toString
	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}

	// Getters y Setters
	public double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	
	
	
	
	
}
