import java.util.Scanner;

import model.Electrodomestico;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("De que color quieres el electrodomestico?");
		System.out.println("Blanco, negro, rojo, azul o gris");
		String color = teclado.next();
		
		System.out.println("Y la eficiencia energética?");
		System.out.println("A, B, C, D, E o F");
		char letra = teclado.next().charAt(0);
		
		Electrodomestico electrodomestico1 = new Electrodomestico();
		Electrodomestico electrodomestico2 = new Electrodomestico(550.50, 50.3);
		Electrodomestico electrodomestico3 = new Electrodomestico(1550.5, color, letra, 25.3);
		
		System.out.println(electrodomestico1);
		System.out.println(electrodomestico2);
		System.out.println(electrodomestico3);
	}

}
