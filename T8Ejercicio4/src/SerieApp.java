import model.Serie;

public class SerieApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Serie serie1 = new Serie();
		Serie serie2 = new Serie("Prison Break", "Paul Scheuring");
		Serie serie3 = new Serie("Sons of Anarchy", 7, "Drama / Crimen", "Kurt Sutter");
		
		System.out.println(serie1);
		System.out.println(serie2);
		System.out.println(serie3);
	}

}
